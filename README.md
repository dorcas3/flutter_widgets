# flutterwidgets

Flutter Widgets

## Description

A flutter Application that demonstrates the use of layouts, forms and routes

## Screenshots Preview

![Image Preview](assets/images/page12.png)

## Getting Started

### User Requirements

- Build an application that contain 3 pages
- Page one should contain a A splash page that will display an image and the name of the application. Centered both horizontally and verticall
- Page two should  present a form with 2 input fields; a text field, a select field and a button. The text field should receive your name. The select field should give options of all the stacks available in your internship program
- Page 3 should  display a grid view of all the stacks that are available. In each grid, you should list the environment for the stack. E.g flutter - mobile, vuejs - frontend etc.

## Prerequisites
- Flutter v2.2.0

## Setup and Installations

- Clone the repository into your local machine
- In the terminal type flutter doctor to install all the dependencies
- Type flutter run command to launch the application

### License

* LICENSED UNDER  [![License: MIT](https://img.shields.io/badge/License-MIT-yellow.svg)](license/MIT)
