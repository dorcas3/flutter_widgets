import 'package:flutter/material.dart';

class ThirdPage extends StatefulWidget {
  @override
  _ThirdPageState createState() => _ThirdPageState();
}

class _ThirdPageState extends State<ThirdPage> {
  // List<Language> stacks = [
  //   Language(stack:'',environ:'')
  // ];

  List stacks = [
    ['flutter', 'assets/images/f.png','Mobile'],
    ['Adonis', 'assets/images/adonis.jpeg','Backend'],
    ['Vue', 'assets/images/vue.png','Frontend'],
    ['Quarks', 'assets/images/quarkus.png','Backend'],
    ['Go', 'assets/images/go.png','Backend'],
    ['Sql', 'assets/images/sql.png','Database'],
  
  ];

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Grid Widget',
            style: TextStyle(color: Colors.white),
          ),
          //backgroundColor: Colors.blue[400],
        ),
        body: Container(
          child: Padding(
            padding: const EdgeInsets.fromLTRB(10.0, 40.0, 10.0, 0.0),
            child: GridView.count(
              crossAxisCount: 2,
              childAspectRatio: (2 / 3),
              children: stacks
                  .map((data) => GestureDetector(
                      child: Card(
                          elevation: 5.0,
                          margin:
                              EdgeInsets.symmetric(vertical: 5, horizontal: 5),
                          color: Colors.teal[50],
                          child: Center(
                              child: Column(
                            children: [
                              Image.asset(data[1]),
                              Text(data[0],
                                  style: TextStyle(
                                      fontSize: 22,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w700),
                                  textAlign: TextAlign.center),
                              Text(data[2],
                                  style: TextStyle(
                                      fontSize: 15,
                                      color: Colors.black,
                                      fontWeight: FontWeight.w300),
                                  textAlign: TextAlign.center),
                            ],
                          )))))
                  .toList(),
            ),
          ),
        ),
        backgroundColor: Colors.grey[200],
      ),
    );
  }
}
