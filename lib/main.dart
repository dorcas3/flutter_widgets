import 'package:flutter/material.dart';
import 'secondpage.dart';
import 'thirdpage.dart';


void main() {
  runApp(MaterialApp(
    debugShowCheckedModeBanner: false,
    initialRoute: '/',
    routes: {
      '/': (context) => SecondPage(),
      '/third': (context) => ThirdPage()
    },
  ));
}


