import 'package:flutter/material.dart';

class SecondPage extends StatefulWidget {
  @override
  _SecondPageState createState() => _SecondPageState();
}

class _SecondPageState extends State<SecondPage> {
  var stack;
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            'Forms Widget',
            style: TextStyle(color: Colors.white),
          ),
          backgroundColor: Colors.blue[400],
        ),
        body: Padding(
          padding: EdgeInsets.fromLTRB(30.0, 40.0, 40.0, 0.0),
          child: Form(
            child: Column(
              children: [
               Image.asset('assets/images/widgets.jpeg'),
                Padding(
                  
                  padding: const EdgeInsets.fromLTRB(20.0, 30.0, 20.0, 50.0),
                  child: Text('Signup Form',style: TextStyle(color: Colors.blueAccent,fontSize: 30.0),),
                ),
                TextFormField(
                  decoration: InputDecoration(
                      icon: Icon(Icons.person), hintText: 'Enter Your Name'),
                ),

                SizedBox(
                  height: 40.0,
                ),
                DropdownButtonFormField<String>(
                  decoration: InputDecoration(
                    icon: Icon(Icons.person),
                    labelText: 'Select Stack',
                  ),
                  value: stack,
                  items: ['Adonis', 'Flutter', 'Quarkus', 'GoLang', 'Vue']
                      .map((data) => DropdownMenuItem(
                            child: Text(data),
                            value: data,
                          ))
                      .toList(),
                  onChanged: (value) {
                    setState(() {
                      stack = value;
                    });
                  },
                ),
                SizedBox(
                  height: 40.0,
                ),
                Padding(
                  padding: const EdgeInsets.fromLTRB(30.0, 50.0, 20.0, 0.0),
                  child: FloatingActionButton.extended(
                    onPressed: () {
                      Navigator.pushNamed(context, '/third');
                    },
                    label: Text(
                      'Submit',
                      style: TextStyle(fontSize: 20.0),
                    ),
                    icon: Icon(Icons.save),
                  ),
                )
              ],
            ),
          ),
        ),
      ),
    );
  }
}
